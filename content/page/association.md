---
date: 2018-06-01T00:00:35Z
title: Association
menu: main
weight: -100
url: /association/
---

Depuis juillet 2016, le collectif d'artistes est structuré en association loi 1901 reconnue d’intérêt général.

Soutenir l'association IDLV : [{{< mark "ADHÉRER" >}}](https://idlv.co/adherez) ou [{{< mark "FAIRE UN DON" >}}](https://idlv.co/donnez)

- Avec cette adhésion, vous soutiendrez les projets artistiques portées par l'association IDLV  (le don et l'adhésion ouvrent droit à une réduction fiscale de 66% car ils remplissent les conditions générales du code général des impôts).
- En devenant bénévole, vous serez tenu au courant et pourrez participer à la vie associative, expérimentant et développant ensemble une communauté afin d'explorer dans le partage et l'échange !

L'association a pour objet :

- Favoriser l’accès à l’art et la culture au plus large public possible.
- Imaginer la ville et le monde de demain, avec une conscience éthique, sociétale et environnementale.
- Amener un rapport à la technique engagé, inventif et artistique.
- Encourager l'exploration des intersections entre domaines traditionnellement séparés.

Le Bureau :

- Président : Mathias HERARD
- Trésorière : Stéphanie MELOT
- Trésorière Adjointe : Anaëlle GIULIANI
- Secrétaire : Mélodie WESSELS
- Représentant des artistes : Marin ESNAULT

Nos soutiens et nos partenaires : <p><a href="http://www.hotelpasteur.fr/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/pasteur-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://myhumankit.org/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/mhk-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/LabBroPondi/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/LAB-BRO-PONDI-2.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://www.labfab.fr/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/labfab-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://makeme.fr/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/makeme-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://keureskemm.fr/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/keureskemm-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://www.electroni-k.org/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/electronik-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.ateliersmedicis.fr/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/ateliers-m-dicis-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://www.pingbase.net/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/ping-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="https://lesautrespossibles.fr/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/lesautrespossibles-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.folk-paysages.fr/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/folk.jpg"></a></p>

Après 3 ans de travail, depuis janvier 2022 l'Atelier Commun vol de ces propres ailes, retrouver toutes les infos sur le projet sur le site web [ici](https://lateliercommun.co/) !

<p><a href="https://lateliercommun.co/" target="newtab" style="display: inline-block;"><img src="/images/2023/03/association/logo-02.png"></a></p>