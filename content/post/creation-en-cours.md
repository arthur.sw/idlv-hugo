+++
author = "quentin.orhant@gmail.com"
date = 2017-01-02T00:44:00Z
image = "/images/2017/05/83.jpg"
description = ""
draft = false
slug = "creation-en-cours"
title = "La ville aux enfants"

+++

<br>

![2-1](/images/2017/12/2-1.jpg)

<br>

Fin 2016, les Ateliers Médicis initient avec le ministère de la Culture et de la Communication et le ministère de l’Éducation Nationale, de l’Enseignement Supérieur et de la Recherche, le dispositif «Création en cours» qui vise à soutenir les artistes émergents et à favoriser des résidences en milieu scolaire dans les territoires les plus éloignés de l’offre culturelle.

Lauréat, le collectif Indiens Dans La Ville propose alors aux enfants de cycle 3 de l’école élémentaire publique Victor SCHOELCHER de la ville de Guer (56) de travailler sur l’observation et l’analyse de la Ville d’aujourd’hui pour ensuite imaginer et esquisser ce que pourrait-être la «Ville de demain».

L’intention de ce projet visait à inverser la manière d’envisager la conception de l’environnement urbain, en donnant la possibilité à des enfants de 10 ans de s’approprier des outils et des méthodes utiles à l’observation et à la compréhension de l’aménagement citadin.

Valorisant l’intelligence collective, encourageant la mise en commun autant que la liberté d’expression et de jugement, afin de favoriser auprès des élèves le développement et la prise en compte d’une vision d’ensemble. Cette résidence rythmée de travaux d’écriture, de dessin, de maquette et d’une série de débats, prend sa forme finale à travers cette édition, témoignant de la volonté de ces élèves de participer à l’aménagement de leur environnement présent et futur.

</div>

<br>
<center>
    
<div data-configid="8103426/56593189" style="width:640px; height:454px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>

</center>

<br>

{{< mark "Sources :" >}}

* <a href="https://www.dropbox.com/s/0un4ux3vpc4m20b/PR%C3%89SENTATION%20ARTISTIQUE%20D%C3%89VELOPP%C3%89E%20DU%20PROJET%20DE%20RESIDENC2.pdf?dl=0" traget="newtab">Présentation artistique développée du projet de résidence  en pdf</a>
* <a href="https://creationencours.fr/projet/la-ville-aux-enfants/" traget="newtab">Wordpress du projet</a>






