+++
author = "charly@idlv.co"
date = 2019-06-06T10:55:33Z
image = "/images/2023/03/projet-lucioles/projet-lucioles.jpg"
description = ""
draft = false
slug = "projet-lucioles"
title = "Projet Lucioles"
+++

![lucioles-bandeau](/images/2023/03/projet-lucioles/LUCIOLES_visuel_bandeau.png)
<div style="center">Il existe un insecte qui fait rêver et nous transporte dans nos souvenirs d’été.
Cet insecte aujourd’hui se fait rare et peu d’enfants ont eu la chance de le voir « pour de vrai ».</div>

Début 2019, c’est dans le cadre de l’expérience culturelle de « l’Année de la Vilaine » menée au travers du projet de valorisation de la vallée de la Vilaine portée par Rennes Métropole et les 7 communes de la vallée, que la paysagiste rennaise Adélaïde Fiche et le collectif d’artistes pluridisciplinaires Indiens dans la ville, lancent avec la coopérative culturelle Cuesta, le projet Lucioles.

Motivés par l’envie de sensibiliser petits et grands à la préservation et la compréhension de l’environnement, c’est à travers la découverte et l’attachement à cet insecte* fascinant qui disparaît petit à petit à cause de la modification des milieux naturels et de la pollution lumineuse que nous avons choisi d’unir les forces et les idées pour susciter le questionnement auprès des habitants et des enfants de la métropole rennaise, et au-delà, sur l’importance de protéger les milieux et la biodiversité associée.

![lucioles-bandeau](/images/2023/03/projet-lucioles/Firefly_Hotaru.jpg)

{{< mark "UN PROJET EN 4 PHASES : DE L’ENQUÊTE A LA RÉIMPLATATION" >}}

Où sont les Lucioles sur la Vallée de la Vilaine ? Qui en a déjà vu ? Où ? Quand ?  Quelles sont les conditions de leur présence ? En quoi sont-elles une espèce sentinelle? Comment les faire revenir ?De février à juin 2019, que vous soyez expert ou curieux, venez contribuer en ligne à la première phase de cette enquête en apportant votre témoignage ou toute autre information susceptible d’agréger progressivement une somme importante de connaissances sur les lucioles et leur présence dans la Vallée de la Vilaine. Racontez nous vos souvenirs, récoltez les anecdotes de vos proches, inscrivez sur une carte les endroits où vous savez qu’elles vivent, parlez-nous de bioluminescence, de paysage, d’insecte, de géologie, d’anthropisation, d’histoire …Un blog (https://lucioles.blog/), créé spécialement pour faciliter la mise en commun sur le sujet permettra de centraliser les informations collectées afin de faire émerger des problématiques fortes, qui serviront de matière pour le  premier « hackathon luciole » ouvert à tous, qui se tiendra les 13 et 14 juin prochains.
Sous réserve de faisabilité scientifique, cette enquête sera suivie l’année prochaine d’une série d’expérimentations co-construites visant à réimplanter des vers-luisants dans des « zones-test » de la Vallée.

{{< mark "UN PROJET COLLABORATIF ET CITOYEN" >}}

Chaque personne doit pouvoir s’impliquer dans la production et dans la compréhension de son environnement. Ainsi, c’est avec l’ambition d’amener chacun à porter un nouveau regard sur ce qui l’entoure au quotidien, que nous souhaitons inviter et encourager le plus grand nombre à participer et contribuer à cette initiative de manière à multiplier les approches, les références et les idées.
Inspiré des sciences participatives, ce projet sur le long terme et le blog qui l’accompagne nécessite donc  d’être le plus largement diffusés auprès des citoyens comme des communautés spécialisées. Nous en appelons donc à vos expertises mais aussi à votre réseau de manière à faire de ce projet un projet d’envergure.

{{< mark "Sources :" >}}

- Pour en savoir plus et participer, rendez-vous sur le site https://lucioles.blog/
- Le site de la Vallée de la Vilaine : https://valleedelavilaine.fr