+++
author = "charly@idlv.co"
date = 2022-06-06T10:55:33Z
image = "/images/2023/03/sous-un-autre-regard/sous-un-autre-regard.jpg"
description = ""
draft = false
slug = "sous-un-autre-regard"
title = "sous-un-autre-regard"
+++

Le projet Sous Un Autre Regard, a été imaginé avec la volonté d’utiliser l’art comme support d’une expérience commune afin de faire se rencontrer les adolescents du GPAS du Val d’Ile (35) et ceux du service La Passerelle de l’IME EDEFS 35 autour du thème du portrait et de la représentation de soi.

Quel regard porte-t-on sur soi et sur l’autre ? 
Comment se représente-t-on ?

![dessins](/images/2023/03/sous-un-autre-regard/dessins.jpg)
![portraits](/images/2023/03/sous-un-autre-regard/portraits.png)

Réunis sous le collectif «G.P.P.I.S», Baptiste, Charlie, Dylan, Ewen, Hugo, Katell, Maelys et Melvin et leurs encadrantes Anaïs, Jade, Maïwenn (la Passerelle) et Arianne et Adeline (GPAS) ; auront découvert l’origine des techniques de gravure et se seront initiés aux différentes phases nécessaires à la réalisation d’une série d’impressions artisanales (dessin, gravure, composition, tirage) via l’utilisation d’une presse d’impression XXL  (capable de réaliser des formats de la taille des affiches d’arrêts de bus : 120x176cm) imaginée par les artistes du collectif IDLV.

![galerie](/images/2023/03/sous-un-autre-regard/galerie.png)