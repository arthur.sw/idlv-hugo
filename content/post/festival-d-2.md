+++
author = "Charly2"
date = 2015-09-25T22:09:00Z
image = "/images/2016/09/06_couv-festival-D-1.jpg"
description = ""
draft = false
slug = "festival-d-2"
title = "Festival D"

+++

Les 26 et 27 septembre, au Lieu Unique à Nantes pour le Festival D organisé par l’association PING, IDLV propose une installation originale :

Par l’action simultanée de deux traceurs verticaux, la représentation cartographique des modes de transports qui entourent le lieu d’exposition illustre et confronte les différentes manières de se déplacer dans la ville.

<br>

![](/images/2017/05/Double.jpg)

<br>

{{< pswp-init >}}

{{< gallery >}}

{{< figure link="/images/2017/02/DSC4338.jpg" thumb="-thumb" size="1500x993" >}}
{{< figure link="/images/2017/02/DSC4364.jpg" thumb="-thumb" size="1500x993" >}}
{{< figure link="/images/2017/02/DSC4378--3-.jpg" thumb="-thumb" size="1500x993" >}}
{{< figure link="/images/2017/02/DSC4383--3-.jpg" thumb="-thumb" size="1500x993" >}}
{{< figure link="/images/2017/02/DSC4386--3-.jpg" thumb="-thumb" size="1500x993" >}}
{{< figure link="/images/2017/02/DSC4421--3-.jpg" thumb="-thumb" size="1500x993" >}}
{{< figure link="/images/2017/02/DSC4521--3-.jpg" thumb="-thumb" size="1500x993" >}}
{{< figure link="/images/2017/02/DSC4522--3-.jpg" thumb="-thumb" size="1500x993" >}}

{{< /gallery >}}


{{< mark "Sources :" >}}

* <a href="http://www.festivald.net/" target="_blank">Le site de Festival D</a>
* <a href="https://www.openstreetmap.org/#map=12/47.2151/-1.5391" target="_blank">OpenStreetMap</a>

