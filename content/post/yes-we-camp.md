+++
author = "Charly2"
date = 2014-04-15T08:51:00Z
image = "/images/2016/09/03_couv-marmites-1.jpg"
description = ""
draft = false
slug = "yes-we-camp"
title = "Yes we camp"

+++

Pour notre venue à Nanterre, à l’occasion du festival Les Marmites Artistiques, nous poursuivons notre démarche en transformant le campus universitaire en véritable campement. Le projet « Yes, we camp » a pour objectif de modifier l’aménagement paysager en un espace intimiste et convivial.

Trois tipis disposés dans le patio du bâtiment principal invitent les étudiants à s’approprier le lieu. Au centre, un totem construit en matériaux de récupération arbore les symboles de la modernité de notre société.


{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/05/0003.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/008.jpg" thumb="-thumb" size="1000x667" >}}
{{< figure link="/images/2017/05/0002.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/0009.jpg" thumb="-thumb" size="1000x665" >}}
{{< figure link="/images/2017/05/0004.jpg" thumb="-thumb" size="1000x665" >}}
{{< figure link="/images/2017/05/002.jpg" thumb="-thumb" size="1000x667" >}}
{{< figure link="/images/2017/05/0014.jpg" thumb="-thumb" size="1000x666" >}}
{{< figure link="/images/2017/05/IMGP9430.jpg" thumb="-thumb" size="1000x665" >}}

{{< /gallery >}}


