+++
author = "Charly2"
date = 2014-10-02T22:10:00Z
image = "/images/2016/09/04_couv-tamtam-1.jpg"
description = ""
draft = false
slug = "soiree-tamtam-2"
title = "TAMTAM"

+++


<center>
    <iframe src="https://player.vimeo.com/video/108098055" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <br>
    <a href="https://vimeo.com/108098055">IDLV &agrave; TamTam</a>
</center>

<center>Musique : Bonobo - Scuba </center>

<br>

A l'occasion de la soirée «TAMTAM», nous mettons en place un jeu de lumière participatif sur la façade de la tour de la sécurité sociale.

Le public est invité à se mettre en scène, en s'inspirant du travail photographique effectué lors de la projection de l'année précédente. Utilisant des objets de la culture des indiens d'Amérique fabriqués avec le marionnettiste Fabien Moretti, les participants doivent alors se placer devant le projecteur pour voir leur propre silhouette déguisée apparaître sur la façade du bâtiment.

Des tipis sont une nouvelle fois tendus dans l’espace public, conviant les passants à s’installer autour d’un monticule de pierres symbolisant le feu et la convivialité tout en profitant du spectacle qu’offrent les ombres chinoise.


{{< gallery >}}
{{< figure link="/images/2017/05/DSC_0213.jpg" thumb="-thumb" size="1000x667" >}}
{{< figure link="/images/2017/05/DSC_0308.jpg" thumb="-thumb" size="1000x667" >}}
{{< figure link="/images/2017/05/DSC_0303.jpg" thumb="-thumb" size="1000x667" >}}
{{< figure link="/images/2017/05/DSC6214.jpg" thumb="-thumb" size="1000x667" >}}

{{< /gallery >}}

