+++
author = "charly@idlv.co"
date = 2022-09-06T10:55:33Z
image = "/images/2023/03/comme-un-dessein-hotel-pasteur/thumbnail.jpg"
description = ""
draft = false
slug = "comme-un-dessein-hotel-pasteur"
title = "Comme un dessein à l'Hôtel Pasteur"

+++

![comme-un-dessein-hotel-pasteur-fresque](/images/2023/03/comme-un-dessein-hotel-pasteur/fresque.png)

Cinquième fresque du projet [Comme Un Dessein](idlv.co/commeundessein/), c'est le plus long des projets Comme Un Dessein imaginé par le collectif IDLV.

Initié suite à une demande de la conciergerie de l'Hôtel Pasteur cette fresque, témoin sensible de la participation des personnes qui gravitent et participent au fonctionnement du lieu, fait office de marqueur temporel et contribue à raconter d’une autre manière le lien entre le bâtiment et ses usager.e.s dans le temps.
Cette édition aura durée 3 mois, recueillie 577 dessins et 2912 votes positifs ou négatifs (plus de 13046 traits) pour que soit composée progressivement cette grande fresque (de presque 7 mètres de long) grâce à la participation des petits comme des grands.

{{< mark "Une oeuvre collective et collaborative à l’Hôtel Pasteur :" >}}

Le projet Comme Un Dessein tend à expérimenter la capacité d'une communauté de géométrie variable (citoyen.ne.s, membres d'une organisation, ...) à converger vers la création d’une œuvre qui conviendrait à tous.
Qu’il soit artiste, néophyte, observateur, critique ou simplement curieux, chacun et chacune peut contribuer à sa manière (en dessinant autant qu'il/elle le souhaite, en votant autant qu'il/elle le souhaite ou les deux) pour contribuer à faire émerger collectivement sur une durée variable (ici 3 mois) à une œuvre finale.
Ainsi, la modération n'est plus un facteur limitant mais bien un catalyseur de création collective qui, à force de validation, d'invalidation et de compromis, et sans autorité centrale, générera un résultat de facto représentatif du processus démocratique.

![comme-un-dessein-hotel-pasteur](/images/2023/03/comme-un-dessein-hotel-pasteur/comme-un-dessein-6.jpg)

{{< mark "La concertation :" >}}

Présenté en amont de son lancement auprès de l’équipe pédagogique de l’école, auprès de l’inspection éducation nationale, de l’équipe de l’édulab et de l’équipe du périscolaire. Le cahier des charges à été élaboré de manière concertée pour imaginer les différents types de contributions et s’assurer de la plus grande participation possible.

A noter : le principe du vote a été appréhendé par les enseignants et accompagné dans le cadre de la classe, les élèves pouvaient déposer leurs dessins dans une boîte prévue à cet effet. Les dessins étaient ensuite récupérés par les artistes du collectif IDLV puis fidèlement recopiés afin de permettre aux enfants de participer bien que n’ayant pas encore été habitués à l’utilisation d’un site internet complexe.

{{< pswp-init >}}
{{< gallery >}}

{{< figure link="/images/2023/03/comme-un-dessein-hotel-pasteur/comme-un-dessein-1.jpg" thumb="-thumb" size="2000x1333" >}}
{{< figure link="/images/2023/03/comme-un-dessein-hotel-pasteur/comme-un-dessein-2.jpg" thumb="-thumb" size="2000x1333" >}}
{{< figure link="/images/2023/03/comme-un-dessein-hotel-pasteur/comme-un-dessein-3.jpg" thumb="-thumb" size="2000x1333" >}}
{{< figure link="/images/2023/03/comme-un-dessein-hotel-pasteur/comme-un-dessein-4.jpg" thumb="-thumb" size="2000x1333" >}}
{{< figure link="/images/2023/03/comme-un-dessein-hotel-pasteur/comme-un-dessein-5.jpg" thumb="-thumb" size="2000x1333" >}}


{{< /gallery >}}




Présenté en amont de son lancement auprès de l’équipe pédagogique de l’école, auprès de l’inspection éducation nationale, de l’équipe de l’édulab et de l’équipe du périscolaire. Le cahier des charges à été élaboré de manière concertée pour imaginer les différents types de contributions et s’assurer de la plus grande participation possible.
