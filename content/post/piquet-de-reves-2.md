+++
author = "Charly2"
date = 2015-09-11T22:10:00Z
image = "/images/2017/05/pik.jpg"
description = ""
draft = false
slug = "piquet-de-reves-2"
title = "Piquet de rêves"

+++

Alors qu’approche la fin de l’été 2015, la galaxie 1702 invite les rennais à s’approprier les espaces nouvellement créés du Mail François Mitterrand, pour la première session de l’évènement : “Piquet de Rêves # 1 La plage”.

Cette intervention est pour le collectif l’occasion de réaliser pour la première fois une fresque dans l’espace public grâce au robot qui dessine, fabriqué durant l’année.  

Laissant place à la contemplation et à la découverte, dessinant un à un les différents espaces de la ville ; une vingtaine d’heure ont été nécessaire pour que le robot réalise une fresque surréaliste qui représente Rennes au bord de la mer. 
Le lendemain les habitants sont invités à prendre part à la création en venant colorier la fresque, équipés de leurs crayons, pinceaux et pots de peinture.

Mail François Mitterrand - Rennes
11-12-13 sept. 2015

![](/images/2017/05/1-originale.jpg)

<br>

{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/05/DSC3918.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC3938.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC4150.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC4007.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC4072.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC4170.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC4254.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2017/05/DSC4262.jpg" thumb="-thumb" size="1000x662" >}}

{{< /gallery >}}



