+++
author = "Charly2"
date = 2013-10-01T08:44:00Z
image = "/images/2016/09/02_couv-projections.jpg"
description = ""
draft = false
slug = "projections-dans-lespace-public-2"
title = "Projections"

+++

Le 1er octobre 2013, le collectif investi le centre-ville de Rennes dans le cadre de la 7ème édition du festival L’Image Publique. 

A travers sept photographies de silhouettes projetées en grands formats sur les immeubles modernes du quartier colombier, le projet consiste à recréer in-situ le travail de superpositions élaboré dans le deuxième fanzine IDLV. Utilisant l’espace public pour susciter la curiosité et questionner le spectateur sur la place de l’homme en milieu urbain.

Le vendredi 8 novembre de la même année, les indiens investissent Chantepie et ses nouveaux quartiers :  "Alors que les grands ensembles sortent de terre parmi les chênes qui rythmaient autrefois nos campagnes, la représentation symbolique des indiens dans cet environnement incite les habitant des quartiers de la Touche Annette et des Neuf Journaux à se questionner sur leur cadre de vie et la façon dont nous nous approprions l’espace public..."

<br>

![](/images/2017/05/flyer-IDLV--3-.jpg)

<br>

{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/02/003-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/002-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/004-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/005-PH.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/006-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/007-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/008-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/009-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/012-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/013-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/014-JM.jpg" thumb="-thumb" size="500x500" >}}
{{< figure link="/images/2017/02/015-JM.jpg" thumb="-thumb" size="500x500" >}}

{{< /gallery >}}