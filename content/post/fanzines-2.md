+++
author = "Charly2"
date = 2012-12-12T22:36:00Z
image = "/images/2017/05/fanzine.jpg"
description = ""
draft = false
slug = "fanzines-2"
title = "Fanzines"

+++

<center>
    <iframe src="https://player.vimeo.com/video/61091882" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <br>
    <a href="https://vimeo.com/61091882">Le making-of (teaser) du Fanzine IDLV #2</a>
</center>

<center>Musique : The shaolin afronauts - Kilimanjaro </center>

En décembre 2012, le collectif réalise un premier fanzine « Indiens Dans La Ville », magazine amateur tiré à cinquante exemplaires. Issu du projet intitulé « Back to the futur », ce photozine de 9 feuillets illustre au moyen de la technique de la « rephotographie » une série de photomontages réalisée à partir de clichés de la ville de Rennes. Par un jeu d’association et de composition, les bâtiments d’époque se retrouvent superposés aux nouvelles lignes de l’architecture contemporaine. 

Quatre mois plus tard paraît le deuxième numéro du fanzine IDLV. Une série d’images représentant des silhouettes d’indiens sont superposées sur différents bâtiments modernes du centre-ville rennais par la technique photographique de la double exposition.
<br>

Fanzine # 1

{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2016/10/1-1-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/1-2-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/1-3-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/1-4-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/1-5-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/1-6-1.jpg" thumb="-thumb" size="1000x663" >}}

{{< /gallery >}}


Fanzine # 2

{{< gallery >}}

{{< figure link="/images/2016/10/2-0-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/2-1-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/2-2-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/2-3-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/2-4-1.jpg" thumb="-thumb" size="1000x663" >}}
{{< figure link="/images/2016/10/2-5-1.jpg" thumb="-thumb" size="1000x663" >}}

{{< /gallery >}}


