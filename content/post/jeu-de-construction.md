+++
author = "quentin.orhant@gmail.com"
date = 2016-01-18T18:00:00Z
image = "/images/2017/06/jeu-de-construction-miniature-1.jpg"
description = ""
draft = false
slug = "jeu-de-construction"
title = "Jeu de construction"

+++

{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/06/P6180516-1.jpg" thumb="-thumb" size="1000x687" >}}
{{< figure link="/images/2017/06/P6180523-1.jpg" thumb="-thumb" size="1000x687" >}}
{{< figure link="/images/2017/06/P6180525-1.jpg" thumb="-thumb" size="1000x687" >}}

{{< /gallery >}}

{{< mark "Sources :" >}}

* <a href="https://www.thingiverse.com/thing:2391921" target="_blank">Les fichiers 3D</a> 
