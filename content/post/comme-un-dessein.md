+++
author = "quentin.orhant@gmail.com"
date = 2017-09-06T10:55:33Z
image = "/images/2017/09/commeundessein.jpg"
description = ""
draft = false
slug = "comme-un-dessein"
title = "Comme un dessein"

+++

<br>

<center>

<iframe src="https://player.vimeo.com/video/240647274" class="center" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<a align="center" href="https://vimeo.com/240647274">Festival Maintenant - 10 au 15 octobre 2017
Théâtre du Vieux St-Etienne - Rennes (Fr)</a>

</center>

<br>

>« La démocratie participative fait-elle réellement émerger l’intérêt général ? »

L’installation Comme un dessein fait écho à cette question actuelle et invite le public, du 10 au 15 octobre 2017 à l'occasion du festival Maitenant, à débattre et à proposer des dessins qui viendront progressivement composer une fresque collective grâce à une interface web connectée à un traceur vertical disposé dans le Théâtre du Vieux St-Étienne.

Une proposition du collectif IDLV, accompagnée par la Ville de Rennes au titre du dispositif « Les rennais prennent l'art » et coproduite par Electroni[k], en réponse à l’appel à projet international Arts & Technologies 2016.

<br>

![](/images/2017/09/LOGO-LRPLA-noir-1.jpg)
<br>

![](/images/2017/09/BLOCKMARK_NOIR_CMJN-1.jpg)
<br>

![](/images/2017/09/Comme-un-dessein-01.jpg)
<br>

{{< mark "Expérimentation :" >}}

>« Internet favorise-t-il réellement le débat démocratique et la prise de décision collective ? »

Fruit de la concertation commune, le projet tend à faire converger le public vers la création d’une œuvre qui conviendrait à tous et se présenterait donc comme le résultat de la volonté générale. Par le biais de l’Internet, le dispositif a vocation à favoriser les échanges et les interactions de manière à faire émerger dans l’espace public une proposition commune.

Le public sera donc invité à contribuer à l’élaboration d’une œuvre d’ensemble, qui sera simultanément réalisée sur un mur virtuel d’expression libre et restituée dans l’espace public grâce à un robot qui dessine. Chaque dessin proposé par les internautes sera soumis à validation communautaire à travers une plateforme de modération accessible à tous. Qu’il soit artiste, néophyte, observateur, critique ou simplement curieux, chacun devra décider, débattre et voter pour les dessins qui seront à garder ou à reléguer de manière à faire émerger l’œuvre finale.

Ainsi, la modération ne sera plus un facteur limitant mais bien un catalyseur de création collective qui, à force de censures et de compromis, et sans autorité centrale, générera un résultat de facto représentatif du processus démocratique.

{{< mark "Source :" >}}

* <a href="https://commeundessein.co" target="_blank">commeundessein.co</a>
* <a href="https://www.maintenant-festival.fr/2017/programmation/" target="_blank">Programmation du festival Maintenant 2017</a>
* <a href="https://www.dropbox.com/s/12qfk5hvgaevpo0/Comme%20un%20dessein%20-%20Dossier%20de%20pr%C3%A9sentation.pdf?dl=0" target="_blank">Dossier de présentation du projet</a>