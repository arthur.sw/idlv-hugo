+++
author = "Charly2"
date = 2015-09-25T22:09:00Z
image = "/images/2023/01/cameleon_miniature.png" 
description = ""
draft = false
slug = "quadrichromie"
title = "Quadrichromie"
+++

![ensemble](/images/2023/01/ensemble.png)

<br>

Elaboré à partir de l'outil Trichromatism de l'artiste et développeur [Arthur Masson](http://arthurmasson.xyz/), ces images de représentent des animaux choisis par Arthur et Charly pour recréer et mettre exergue avec patience les beaux plumages et peulages de ces beaux animaux ! 

Composés en s'inspirant de la technique de la quadrichromie : un procédé d'imprimerie permettant de produire par synthèse soustractive une large gamme de teintes à partir de trois teintes dites élémentaires, un bleu (appelé « cyan »), un rouge (« magenta ») et un jaune auxquelles on ajoute le noir ; ces dessins ont été réalisés à l'aide du traceur vertical [Tipibot](https://idlv.co/tipibot/), très utilisé par les membres du collectif IDLV.

<br>

![macareuxjaune](/images/2023/01/macareuxjaune.png)
![cameleonminiature](/images/2023/01/cameleonminiture.png)
![cameleon](/images/2023/01/cameleon.png)

