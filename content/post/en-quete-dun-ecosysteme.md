+++
author = "quentin.orhant@gmail.com"
date = 2017-04-30T21:20:32Z
image = "/images/2017/05/eqde.jpg"
description = ""
draft = false
slug = "en-quete-dun-ecosysteme"
title = "En quête d'un écosystème"

+++

<br>
<center>
<iframe src="https://player.vimeo.com/video/215330739" class="center" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<a href="https://vimeo.com/215330739">En quête d'un écosystème - Laboratoire Artistique Populaire - Avril 2017</a></center><br>




>Émetteur : Collectif Indiens Dans La Ville<br>Destinataire : Participants au Laboratoire Artistique Populaire #152<br>Objet : En quête d’un écosystème<br>Date : 15 mars 2167<br><br>Il fut un temps où nous vivions au rythme des saisons, les autres animaux étaient nos égaux, si ce n'est nos semblables, la flore pouvait aussi bien être notre refuge que notre garde-manger, à cette époque, la nature et l'homme ne faisaient qu'un. Pour nous nourrir, nous savions cultiver la terre pour qu'elle nous fournisse tout ce dont nous avions besoin. L'eau des rivières était potable et les plantes pollinisées par des insectes. Il y eut même un temps où il n'y avait pas de ville, les hommes étaient migrateurs, ou nomades comme ils le disaient. Depuis la Terre nous pouvions encore voir les étoiles que la pollution et la lumière des villes ont depuis occulté.<br><br>Cet équilibre ils l'appelaient écosystème ...<br><br>Peut-être que des vestiges de cette époque subsistent encore, ils pourraient nous permettre de mieux comprendre où nous allons et comment renouer avec la nature plutôt que de seulement la recouvrir de nos villes ? En revêtant le rôle des explorateurs, nous voulons redécouvrir la ville comme si nous ne la connaissions pas, comme si un jour nous étions arrivés là, comme si nous n'en possédions pas la carte, n’en connaissions ni l’histoire, ni le fonctionnement.<br>Nous souhaitons faire l'expérience et l’effort de considérer la ville comme un milieu fragile. En effet, si l'on arrive à accepter que la ville soit un écosystème naturel, alors l'Écosystème urbain peut être un objet d'étude qui nous permettra ainsi de mieux la comprendre.<br><br>Comment favoriser l'équilibre et le vivre-ensemble entre tous les éléments constituants de la ville d’aujourd’hui ?<br><br>La notion d'interdépendance a-t-elle encore une valeur à nos yeux ?


<br>{{< mark "La démarche :" >}}

Pendant une semaine, du 10 au 15 avril 2017, le collectif est parti explorer Rennes avec les participants du Laboratoire Artistique Populaire. Un workshop plein de rencontres et de créativité ... En quête de notre écosystème.

Nous avons souhaité étudier notre environnement sans nous imposer de vérité toute faite, aiguiser notre esprit critique, être attentifs à nos modes vie, d’habiter, de (sur)produire et de (sur)consommer, observer et tenter de comprendre.

Par l’utilisation engagée d’outils open source, nous avons cherché à questionner la ville d’aujourd’hui pour penser la ville de demain. 


<br>{{< mark "Les outils d'explorations :" >}}


{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/04/analyse-de-l-air-2.jpg" thumb="-thumb" size="2000x1374" >}}
{{< figure link="/images/2017/04/cyanotype-2.jpg" thumb="-thumb" size="2000x1374" >}}
{{< figure link="/images/2017/04/Hackathon.jpg" thumb="-thumb" size="2000x1374" >}}
{{< figure link="/images/2017/04/Les-fusains-2.jpg" thumb="-thumb" size="2000x1374" >}}
{{< figure link="/images/2017/05/P5010192.jpg" thumb="-thumb" size="1000x750" >}}
{{< figure link="/images/2017/05/P5010223.jpg" thumb="-thumb" size="1000x750" >}}
{{< figure link="/images/2017/05/P5010197.jpg" thumb="-thumb" size="1000x750" >}}
{{< figure link="/images/2017/05/P5010188.jpg" thumb="-thumb" size="1000x750" >}}

{{< /gallery >}}

<br>{{< mark "Sources :" >}}

* <a href="http://www.instructables.com/id/Air-Pollution-Detector" target="_blank">http://www.instructables.com/Air-Pollution-Detector</a> 
* <a href="http://onfaitout.com/fabriquer-son-fusain-soi-meme" target="_blank">http://onfaitout.com/Fabriquer-son-fusain-soi-meme</a> 
* <a href="http://disactis.com/store/fr/12-cyanotype" target="_blank">http://disactis.com/Cyanotype</a>
* <a href="http://keureskemm.fr/laboratoire-artistique-populaire" target="_blank">http://keureskemm.fr/Laboratoire-artistique-populaire</a> 

<br>

![](/images/2017/05/18156656_1573620195981489_3240036261121620950_o.jpg)