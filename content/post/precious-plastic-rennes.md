+++
author = "quentin.orhant@gmail.com"
date = 2017-02-02T00:40:47Z
image = "/images/2017/05/pp-1.jpg"
description = ""
draft = false
slug = "precious-plastic-rennes"
title = "Precious Plastic Rennes"

+++

Precious Plastic Rennes est une micro-fabrique qui permettra aux rennais de transformer leurs déchets plastiques en de nouveaux objets « précieux ».

Ce projet rennais s’inscrit dans une démarche plus globale, initiée par Dave Hakkens sous le nom de Precious Plastic, visant à favoriser les initiatives locales de valorisation des déchets plastiques. Déjà expérimentée sur tous les continents, cette initiative met à disposition gratuitement les plans et la documentation des machines permettant ce cycle de transformation et d’entrevoir un nouveau système d’échanges. Convaincu par l’importance de la démocratisation des moyens de production et de recyclage à l’échelle locale, une équipe de 6 personnes provenant d’horizons différents œuvre à réaliser ce projet sur le territoire rennais.
![](/images/2017/05/Precious-Plstique.jpg)
{{< mark "Imaginez, demain …" >}}

 - Un lieu pour tous, où vous pourrez venir avec votre bouteille de lait, des couverts et des sacs en plastique, des pots de yaourts… et repartir avec une planche de skate-board, un pot de fleurs, du filament d’imprimante 3D, des perles ou encore un abat-jour… Les ressources sont aussi vastes que les possibilités.

 - Une mallette pédagogique se déplaçant au plus près de vous, de quartier en quartier, d’école en école pour sensibiliser les plus jeunes comme les plus âgés. Un dispositif mobile permettant de visualiser le processus de recyclage du plastique, point de convergence d’une multitude de connaissances et de publics.

 - Un nouveau médium créatif permettant d’expérimenter et de créer avec la volonté d’améliorer sa qualité de vie par une démarche cohérente (de la collecte à la valorisation, en passant par la création).  

Ainsi, vous n’êtes plus un simple consommateur mais un citoyen-acteur d’une ville plus sobre et autonome.

Le collectif IDLV s'associe à <a href="http://labelledechette.com/" target="newtab"> La Belle Déchette</a> pour porter se projet soumis au vote des rennais, pour en savoir plus et voter : <a href="http://tinyurl.com/preciousplasticrennes" target="newtab"> tinyurl.com/preciousplasticrennes</a>

<br>{{< mark "Sources :" >}}

* <a href="http://preciousplastic.com" target="_blank">http://preciousplastic.com</a> 
* <a href="http://labelledechette.com" target="_blank">http://labelledechette.com</a>
* <a href="http://fabriquecitoyenne.rennes.fr" target="_blank">http://fabriquecitoyenne.rennes.fr</a>


![](/images/2017/02/precious-plastic.png)