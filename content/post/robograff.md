+++
author = "Charly2"
date = 2016-06-04T13:09:00Z
image = "/images/2017/05/DSC_2437-large-1.jpg"
description = ""
draft = false
slug = "robograff"
title = "Robograff"

+++

<br>
<center>
    
<iframe src="https://player.vimeo.com/video/185069455" class="center" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<a href="https://vimeo.com/185069455">Robograff - Odyss&eacute;e Urbaine 2016</a></center><center>Musique : Funky DL & Nujabes - Don't even try it</center>

<br>

A l’occasion du Festival Odyssée Urbaine 2016, le collectif IDLV entreprend un projet qui motive depuis leurs début les expérimentations menés avec le robot qui dessine : pour la première fois, le robot utilisera une bombe à peinture et non plus des crayons pour dessiner!

Pour ce projet, le collectif est allé à la rencontre des enfants de l’école primaire rennaise Sonia Delaunay en leur proposant de dessiner la nature dont ils rêvent : des plantes, des arbres, des fleurs… Avec l’intention d’impliquer les enfants dans un processus de création, à travers la découverte d’une pratique artistique et l’usage d’une technologie numérique. Le but de cette initiative est de permettre aux enfants de reconnaître la puissance de leur propre imagination et de les encourager à avoir confiance en leur potentiel créatif.

Après avoir collecté leurs créations, puis les avoir numérisé une à une à l’aide d’une pallette graphique; chaque dessins à été agrandis et fidèlement restitués sur la façade de l’école,  à travers une fresque d’ensemble mettant en valeur l’imaginaire des enfants.

![](/images/2017/05/couv.jpg)<br>


{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2016/11/DSC8243.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/11/DSC8272.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/11/DSC8308.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/11/DSC8339.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/11/DSC8361.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/11/DSC_2441.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/11/DSC_2437-.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/11/DSC_2108-Re-cupe-re-.jpg" thumb="-thumb" size="1000x662" >}}

{{< /gallery >}}


{{< mark "Sources :" >}}

* <a href="http://www.tourisme-rennes.com/fr/agenda/l-odyssee-urbaine-parcours-artistiques-urbains" traget="newtab">Odyssée Urbaine</a>
* <a href="http://www.thingiverse.com/thing:2304214" traget="newtab">Fichier 3D de la nacelle</a>


