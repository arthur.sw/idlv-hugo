+++
author = "quentin.orhant@gmail.com"
date = 2015-10-25T16:03:00Z
image = "/images/2017/05/Sans-titre-4.jpg"
description = ""
draft = false
slug = "tiny-open-metropolis-2"
title = "TinyOpenMetropolis"

+++

Tiny Open Metropolis est un projet de street-art collectif en open source.

L’idée est d’installer des immeubles miniatures dans l’espace public pour créer une métropole à l'échelle du monde. Chacun peut télécharger et imprimer les éléments permettant de construire son immeuble, le placer dans l'espace public et référencer sa participation sur le site dédié.

Pour coller les éléments, le plus simple est avec de l'adhésif double face, si vous avez d’autres solutions, n’hésitez pas à nous transmettre l’info !

Sur la dropbox vous pourrez trouver la dernière version du fichier de travail au format Sketchup, il peut vous être utile pour créer de nouveaux éléments, ou modifier ceux existants.

Twittez vos créations @tinyopenmetro !

<br>

![](/images/2017/05/banni-re1000.jpg)

<br>


{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/06/tom01-1.jpg" thumb="-thumb" size="804x500" >}}
{{< figure link="/images/2017/06/DSC5133-1.jpg" thumb="-thumb" size="800x498" >}}
{{< figure link="/images/2017/06/tom03-1.jpg" thumb="-thumb" size="718x446" >}}
{{< figure link="/images/2017/06/tom04-1.jpg" thumb="-thumb" size="718x446" >}}

{{< /gallery >}}



{{< mark "Sources :" >}}

* <a href="http://www.thingiverse.com/thing:1050679" target="_blank">Les fichiers 3D</a> 
* <a href="http://www.dropbox.com/sh/oxwt142zhbroznl/AAAnYCdUM5-A7cb8Oro577vGa?dl=0" target="_blank">Le dropbox du projet TOM
* <a href="http://tinyopenmetropolis.tumblr.com" target="_blank">Le tumblr du projet TOM</a> 
* <a href="http://twitter.com/tinyopenmetro" target="_blank">Le twitter du projet TOM</a>


